# cities-radiation-comparison

Vergleich der Globalstrahlung in den größten Städten Deutschlands

Dieses Programm ermöglicht den Abruf von Daten zur Sonneneinstrahlung aus [PVGIS](https://re.jrc.ec.europa.eu/pvg_tools/de/) für mehrere Standorte.

## Voraussetzungen und Installation

Es handelt sich um ein Go Projekt, für dessen Kompilierung bzw. Ausführung eine Go 1.19 Entwicklungsumgebung benötigt wird. Danach kann das Programm so installiert werden:

```nohighlight
go install codeberg.org/600watt/cities-radiation-comparison@latest
```

Zusätzlich wird eine CSV-Datei mit Standortdaten benötigt. In diesem Repository ist als Beispiel `cities.csv` enthalten.

## Anwendung

Zum Ausführen des Programms mit den Vorgegebenen Parametern wird es einfach so über die Kommandozeile ausgeführt:

```nohighlight
cities-radiation-comparison
```

Dabei wird eine Datei `cities.csv` im selben Verzeichnis benötigt.

Das Ergebnis wird als Komma-separierte Werte ausgegeben:

```csv
city,yearly irradiation (kWh/m2)
Berlin,961
Dortmund,923
Düsseldorf,936
Frankfurt am Main,966
Freiburg im Breisgau,954
Hamburg,896
Köln,941
Leipzig,969
München,1015
Stuttgart,1011
```

Das lässt sich dann in einer Tabellenkalkulation leicht als Diagramm visualisieren.

![Diagramm](https://codeberg.org/600watt/cities-radiation-comparison/raw/branch/main/docs/einstrahlung-senkrecht.svg)

### Optionen

- `--angle`: Ist diese Option nicht gesetzt, wird die Einstrahlung für eine horizontale Fläche berechnet, was einem Winkel von 90 Grad entspricht. Für eine senkrechte Fläche wird `90` angegeben. Über den Wert `-1` wird für jeden Standort der optimale Neigungswinkel verwendet. Bei allen Neigungen wird stets angenommen, dass die Fläche zum Äquator ausgerichtet ist (also auf der Nordhalbkugel: nach Süden).

- `--horizon`: Hierüber kann angegeben werden, ob das Terrain bzw. der Horizont um den jeweiligen Standort mit in die Berechnung einbezogen werden soll. So können beispielsweise Hügel oder Berge einen späteren Sonnenaufgang oder einen früheren Sonnenuntergang bewirken. Ist die Option nicht angegeben, wird der Horizont berücksichtigt. Mit dem Wert `false` wird dies deaktiviert.

- `--config`: Hiermit kann der Pfad zur Standort-Datei angegeben werden. Falls nicht gesetzt, wird `./cities.csv` genutzt.

Alle Optionen können so angezeigt werden:

```nohighlight
cities-radiation-comparison --help
```

## Datei `cities.csv`

Die Standorte, für welche Daten abgerufen werden sollen, werden in einer Datei im CSV-Format (Komma-separierte Werte) konfiguriert. In diesem Repository ist als Beispiel die Datei `cities.csv` enthalten.

Die erste Zeile der Datei soll Spaltenüberschriften enthalten und wird deshalb ignoriert.

Die Datei muss drei Spalten in dieser Reihenfolge aufweisen:

1. Name des Ortes
2. Breitengrad (latitude)
3. Längengrad (longitude)

Koordinaten werden im System WGS84 angegeben.
