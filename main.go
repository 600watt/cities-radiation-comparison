package main

import (
	"codeberg.org/600watt/cities-radiation-comparison/cmd"
)

func main() {
	cmd.Execute()
}
