package pvgis

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Client struct {
	Endpoint string
}

func New() *Client {
	c := &Client{
		Endpoint: "https://re.jrc.ec.europa.eu",
	}
	return c
}

type Response struct {
	Outputs ResponseOutputs `json:"outputs"`
}

type ResponseOutputs struct {
	Monthly []ResponseOutputsMonthly `json:"monthly"`
}

type ResponseOutputsMonthly struct {
	Year           int     `json:"year"`
	Month          int     `json:"month"`
	Irradiation    float64 `json:"H(i)_m,nullable"`
	IrradiationOpt float64 `json:"H(i_opt)_m,nullable"`
}

// MonthlyIrradiation fetches monthly irradiation averages
// for the time from 2005 to 2020
// for the given location (latitude, longitude) and the
// given surface angle (0 = horizontal) pointing to the equator
func (c *Client) MonthlyIrradiation(latitude, longitude float64, angle int, useHorizon bool) ([]ResponseOutputsMonthly, error) {
	var horizonInt int
	if useHorizon {
		horizonInt = 1
	}

	selectrad := 1
	optrad := 0
	if angle == -1 {
		selectrad = 0
		optrad = 1
		angle = 0
	}

	url := fmt.Sprintf("%s/api/v5_2/MRcalc?lat=%.5f&lon=%.5f&raddatabase=PVGIS-SARAH2&outputformat=json&browser=0&startyear=2005&endyear=2020&usehorizon=%d&selectrad=%d&optrad=%d&angle=%d",
		c.Endpoint, latitude, longitude, horizonInt, selectrad, optrad, angle)

	resp, err := http.Get(url)
	if err != nil {
		return []ResponseOutputsMonthly{}, err
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []ResponseOutputsMonthly{}, err
	}

	var response Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		return []ResponseOutputsMonthly{}, err
	}

	return response.Outputs.Monthly, nil
}

// AverageMonthlyIrradiation returns a yearly irradiation average
// for the given location (latitude, longitude) and the
// given surface angle (0 = horizontal) pointing to the equator
func (c *Client) AverageMonthlyIrradiation(latitude, longitude float64, angle int, useHorizon bool) (float64, error) {
	monthly, err := c.MonthlyIrradiation(latitude, longitude, angle, useHorizon)
	if err != nil {
		return 0, err
	}

	var sum float64
	for _, m := range monthly {
		if m.IrradiationOpt > 0 {
			sum += float64(m.IrradiationOpt)
		} else {
			sum += float64(m.Irradiation)
		}
	}

	return sum / float64(len(monthly)), nil
}
