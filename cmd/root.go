package cmd

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/spf13/cobra"

	"codeberg.org/600watt/cities-radiation-comparison/pkg/pvgis"
)

var (
	citiesFile string
	angle      int
	useHorizon bool
	rootCmd    = &cobra.Command{
		Short: "Calculate global solar radiation for a list of locations/cities",
		Run:   calculate,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	rootCmd.PersistentFlags().StringVar(&citiesFile, "config", "./cities.csv", "cities file path")
	rootCmd.PersistentFlags().IntVarP(&angle, "angle", "", 90, "angle, where 0 is horizontal, -1 is optimal")
	rootCmd.PersistentFlags().BoolVarP(&useHorizon, "horizon", "", true, "use horizon from terrain model")
}

func calculate(cmd *cobra.Command, args []string) {
	dat, err := os.ReadFile(citiesFile)
	if err != nil {
		log.Fatal(err)
	}

	r := csv.NewReader(strings.NewReader(string(dat)))
	var count int

	PVGISClient := pvgis.New()

	fmt.Println("city,yearly irradiation (kWh/m2)")

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		if count > 0 {
			lat, err := strconv.ParseFloat(record[1], 64)
			if err != nil {
				log.Fatal(err)
			}

			lng, err := strconv.ParseFloat(record[2], 64)
			if err != nil {
				log.Fatal(err)
			}

			avg, err := PVGISClient.AverageMonthlyIrradiation(lat, lng, angle, useHorizon)
			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("%s,%.0f\n", record[0], avg*12.0)
		}
		count++
	}

}
